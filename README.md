# WeekPOD. База знаний 

## Разделы:

* [Frontend](frontend/README.md)
* [Backend](backend/README.md)
* [Операционные системы](os/README.md)
* [GTD](gtd/README.md)
* [Изучение иностранных языков](languages/README.md)
* [Финансы](finances/README.md)
* [BlockChain](blockchain/README.md)


## Справка по редактированию MD
* [MD markdown](https://guides.github.com/features/mastering-markdown/)
