# Code snippets. Полезное

[#Home](.././README.md) | [BackEnd](README.md)

[TOC]

* [Основная информация](main.md)
* [Покупка коинов](buy.md)

## merge --squash

- [Тонкости благополучного git-merge](https://habrahabr.ru/hub/git/)
- ```git merge miner-db --squash  --no-commit```

> **git merge --no-commit**

> Это похоже на нормальное слияние, но не создает слияние-фиксацию. Это коммит будет слиянием: когда вы смотрите на историю, ваша фиксация будет отображаться как нормальное слияние.

> **git merge --squash**

> Это объединит изменения в рабочем дереве без создания слияния. Когда вы совершаете объединенные изменения, это будет выглядеть как новая "нормальная" фиксация в вашей ветке: без фиксации слияния в истории. Это почти так же, как вы сделали вишневый выбор во всех сложенных изменениях.

## Swagger

Swagger is a set of open-source tools built around the OpenAPI Specification that can help you design, build, document and consume REST APIs.

* [About Swagger Specification](https://swagger.io/docs/specification/about/)
 | Documentation | Swagger
* http://petstore.swagger.io/?_ga=2.149262454.1594916104.1519528654-478486695.1517807507
* http://editor.swagger.io/?_ga=2.10046067.770216556.1518669242-478486695.1517807507

## GraphQL

GraphQL: A query language for APIs. GraphQL provides a complete description of the data in your API, gives clients the power to ask for exactly what they need and nothing more, makes it easier to evolve APIs over time, and enables powerful developer tools.

* http://graphql.org/learn/
* [Что же такое этот GraphQL?](https://habrahabr.ru/post/326986/)

## Flutter
Flutter is an open-source mobile application development SDK created by Google. It is used to develop applications for Android and iOS, as well as being the primary method of creating applications for Google Fuchsia.[2]
* https://en.wikipedia.org/wiki/Flutter_(software)

```
function findUsersByRole ({
  role, 
  withContactInfo, 
  includeInactive
} = {}) {
  if (role == null) {  
    throw Error(...)
  }
  ...
}

```

```

function requiredParam (param) {
  const requiredParamError = new Error(
   `Required parameter, "${param}" is missing.`
  )
  // preserve original stack trace ...
  
  throw requiredParamError
}

```
 ```

function findUsersByRole ({
  role = requiredParam('role'),
  withContactInfo, 
  includeInactive
} = {}) {...}

```
 ```

function saveUser(userInfo) {
  return pipe(
    validate,
    normalize,
    persist
  )(userInfo)
}

```
 ```

function pipe(...fns) { 
  return param => fns.reduce(
    (result, fn) => fn(result), 
    param
  )
}

```
```
$factoryMock = $this->getMockBuilder(\App\Loaders\Factory::class)
   ->setMethods(['getLoader'])
   ->getMock();
$factoryMock->expects(self::exactly(1))->method('getLoader')
   ->willReturn($loaderMock);

$this->app->instance('loaderfactory', $factoryMock);

App::make('loaderfactory');
```