# Язык Go

[#Home](.././README.md) | [BackEnd](README.md)

[TOC]

Большинство программистов сходятся во мнении, что лучше всего он подходит для создания веб-приложений (в качестве back-end). По сути, применение языка Go ограничивается тремя основными направлениями: сетевое программное обеспечение (прокси?), консольные утилиты и бэкенд.

Плюсы:

* Простота. Небольшой порог вхождения
* Хорошая стандартная библиотека и обширное количество подключаемых
* Компилируемость. Быстрота рядом с C++
* Заточенность под многозадачность

Минусы:

* Нет привычных вещей из ООП
* Нет исключений

Ссылки:

[Язык программирования Go: мнения и перспектива](http://timeweb.com/ru/community/articles/yazyk-programmirovaniya-go-mneniya-i-perspektiva-1)

[За что ругают Golang и как с этим бороться?](https://m.habrahabr.ru/post/282588/)

[Почему вам стоит изучить Go?](https://habrahabr.ru/post/326798/)

[JetBrains Go](https://www.jetbrains.com/go/)

[Target="_blank" - the most underestimated vulnerability ever](https://www.jitbit.com/alexblog/256-targetblank---the-most-underestimated-vulnerability-ever/)