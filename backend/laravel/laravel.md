# Laravel 

[#Home](../.././README.md) | [BackEnd](../README.md)

[TOC]

## Разделы
* [Разное](laravel_misc.md)
* [Middleware](middleware.md)
* [Notifications](notifications.md)
* [Service container](service_container.md)
* [Service providers](service_providers.md)

## Новое в laravel 5.5

* Maillabels. Кастомное оформление писем
* Автоматическая установка пакетов
* migrate:fresh 
* vendor:publish
* Фронтенд пресеты
* Whoops. Красивое отобращение ошибок и открытие IDE на строке с ошибкой
* Кастомные валидации создаются легче
* Методы dd и dupm в коллекциях
* Кастомные blade-директивы
* Авторегистрация новых консольных команд
* Laravel Horizon
* Новый трейт миграии БД


## Полезные материалы

* [Install Laravel on Ubuntu 16.04](https://www.rosehosting.com/blog/install-laravel-on-ubuntu-16-04/)
* [Advanced GIT for Developers - Lorna Jane Mitchell - Laracon EU 2015](https://www.youtube.com/watch?v=duqBHik7nRo)
* https://laracon.net/2017


