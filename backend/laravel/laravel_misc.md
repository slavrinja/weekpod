# Laravel Разное  

[#Home](../.././README.md) | [BackEnd](../README.md)  | [Laravel](laravel.md) 

[TOC]

## Улучшения для консольных команд Laravel

### Mutex

Пакет [Laravel Console Mutex](https://github.com/dmitry-ivanov/laravel-console-mutex) - предотвращает одновременное выполнение одной и той же команды.

Установка

```
composer require illuminated/console-mutex
```

Использование

```php
use Illuminated\Console\WithoutOverlapping;

class ExampleCommand extends Command
{
    use WithoutOverlapping;

    // ...
}
```

Стретегии предотващения:

- `file` (default)
- `mysql`
- `redis`
- `memcached`

Может просто блокировать выполнение команды вызванное при запуске другой, а может дожидатсья указаное время.

Можно для нескольких команд  создать имя мютекса, чтобы можно было общий мьютекс использовать.

### Logging

Пакет [Laravel Console Logger](https://github.com/dmitry-ivanov/laravel-console-logger) предоставляем механизм логирования и извещений для консольных команд. В ларавел вывод штатными средствами происходит в общий лог. Для консольных команд это чаще всего не удобно. Использует все тот же Monolog.

Установка

```
composer require illuminated/console-logger
```

Использование

```php
use Illuminated\Console\Loggable;

class ExampleCommand extends Command
{
    use Loggable;

    public function handle()
    {
        $this->logInfo('Hello World!');
    }

    // ...
}
```

Результат использования

```
[2016-05-11 17:19:21]: [INFO]: Command `App\Console\Commands\ExampleCommand` initialized.
[2016-05-11 17:19:21]: [INFO]: Host: `MyHost.local` (`10.0.1.1`).
[2016-05-11 17:19:21]: [INFO]: Database host: `MyHost.local`, port: `3306`, ip: ``.
[2016-05-11 17:19:21]: [INFO]: Database date: `2016-05-11 17:19:21`.
[2016-05-11 17:19:21]: [INFO]: Hello World!
[2016-05-11 17:19:21]: [INFO]: Execution time: 0.009 sec.
[2016-05-11 17:19:21]: [INFO]: Memory peak usage: 8 MB.
```

Лог будет сохранен в storage/logs в отдельной поддиректории для каждой команды. Также поддерживается ротация логов.



Система извещений имеется на случай если вы хотите быть оперативно проинформированны об сообщениях определенного уровня критичности.

Имеются различные каналы извещений - email, slack, database.

Имеется автоматическое логирование Guzzle 6+ (http-клиент на php).

## Laravel Socialite

Laravel Socialite - Laravel - The PHP Framework For Web Artisans

* https://laravel.com/docs/5.5/socialite
* https://socialiteproviders.github.io/
* https://laravel.com/docs/5.5/queues
* [Exploring Message Brokers](https://dzone.com/articles/exploring-message-brokers)


## Agent (https://github.com/jenssegers/agent)

Пакет для парсинга UserAgent 

**Определение мобилки**

```php
//определениме размеров устройства 
$agent->isMobile();
$agent->isTablet();

//определениме ОС устройства 
$agent->isPhone();
$agent->isAndroidOS();
```

Методы **is** для получения конкретной инфы из UserAgent

```php
$agent->is('Windows');
$agent->is('OS X');
$agent->is('iPhone');
$agent->is('Firefox');
```

определение  платформы и браузера

```php
$browser = $agent->browser();
$bVersion = $agent->version($browser);

$platform = $agent->platform();
$pVersion = $agent->version($platform);
```

Детали девайса для мобилок (iPhone, Nexus, AsusTablet, ...)

```php
$device = $agent->device();
```

Получение языков брауера

```php
$languages = $agent->languages();
// ['nl-nl', 'nl', 'en-us', 'en']
```

Выявление робота

```php
$agent->isRobot();
$robot = $agent->robot();
```



## Laravel Mobile Detect (https://github.com/riverskies/laravel-mobile-detect)

расширение для Blade, помоггаюзее задать вёрстку для разных уcтройств

```php+html
@desktop
    <img src="/path/to/high-definition/image"/>
@enddesktop

@tablet
	<img src="/path/to/handheld-optimised/image"/>
@endtablet

@mobile
	<img src="/path/to/handheld-low/image"/>
@endmobil

```