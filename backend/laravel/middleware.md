# Laravel Middleware

[#Home](../.././README.md) | [BackEnd](../README.md)  | [Laravel](laravel.md) 

[TOC]

## Назначение

Middleware предоставляет механизм фильтрации и пополнительно обработки HTTP запросов. Например, Laravel включает middleware, которое проверяет аутентифицирован ли пользователь. Если нет, то  происходит переадресация к экрану login. Если аутентифицирован, то middleware пропускает обработку далее.

В Laravel уже есть ряд middleware, которые отвечтаю за аутентификацию и CSRF защиту. Все middleware расположены в app/Http/Middleware директории.

# Объявление и реализация

Для создания заготовки есть команда

```
php artisan make:middleware CheckAge
```

Пример:

```php
<?php

namespace App\Http\Middleware;

use Closure;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->age <= 200) {
            return redirect('home');
        }

        return $next($request);
    }
}
```

В примере если age менее чем 200 или равен, то middleware отдаст заголовко редиректа к home. Если более 200, то обработка пойдет далее.

Запрос может проходить через целую цепочку middlewares.

Какие-либо действия middleware может выполнять как до получения ответа от приложения

```php
class BeforeMiddleware
{
    public function handle($request, Closure $next)
    {
        // Perform action

        return $next($request);
    }
}
```

так и после

```php
class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // Perform action

        return $response;
    }
}
```

Регистрация всех middleware происходит в app/Http/Kernel.php

где в $middleware содержится список глобальных middleware

Есть так же middleware которые используются в маршрутах $routeMiddleware

```php
// Within App\Http\Kernel Class...

protected $routeMiddleware = [
    'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
    'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
    'can' => \Illuminate\Auth\Middleware\Authorize::class,
    'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
    'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
];
```

Когда middleware объявлено, мы можем его использовать так

```php
Route::get('admin/profile', function () {
    //
})->middleware('auth');
```

Можно указать несколько

```php
Route::get('/', function () {
    //
})->middleware('first', 'second');
```

Есть возможность создавать группы

```php
/**
 * The application's route middleware groups.
 *
 * @var array
 */
protected $middlewareGroups = [
    'web' => [
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
    ],
```



```php
Route::get('/', function () {
    //
})->middleware('web');
```

Также есть поддержка параметров

```php
<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (! $request->user()->hasRole($role)) {
            // Redirect...
        }

        return $next($request);
    }

}
```

```php
Route::put('post/{id}', function ($id) {
    //
})->middleware('role:editor');
```



##Пример



```php
<?php

namespace App\Http\Middleware;

use App\Libraries\Middleware\Referrer;
use Closure;

class CaptureReferrer
{

    protected $referer;

    public function __construct(Referrer $referer)
    {
        $this->referer = $referer;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->referer->putFromRequest($request);
        return $next($request);
    }
}
```



```php
'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \App\Http\Middleware\CaptureReferrer::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\LocaleMiddleware::class,
        ],
```



## Ссылки

[Документация laravel 5](https://laravel.com/docs/5.5/middleware)
