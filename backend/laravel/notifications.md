# Laravel Notifications

[#Home](../.././README.md) | [BackEnd](../README.md)  | [Laravel](laravel.md) 

[TOC]

## Назначение

Помимо отправки почты Laravel поддерживает отправку уведомлений через различные каналы: SMS (Nexmo), Slack, БД, Broadcast (веб-сокеты)

Для создания таких извещений используется единообразный подход.

## Создание и реализация

Для создания класса извещений используется команда

```
php artisan make:notification InvoicePaid
```

Эта команда создаст новый файл с классом в app/Notifications

В каждом таком классе есть метод via, который содержит перечисление каналов, по которым будет происходить отправка. А также содержатся методы на каждый метод отправки. Типа `toMail` и `toDatabase`.

Чтобы для какой-то сущности можно было слать извещения необходимо использовать специальный трейт

```php
<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
}
```



Когда мы создали класс извещения и сущсность с трейтом, то можно слать так

```php
use App\Notifications\InvoicePaid;

$user->notify(new InvoicePaid($invoice));
```

Или так

```
Notification::send($users, new InvoicePaid($invoice));
```



## Дополнительные возможности



* Можно посылать сразу или с отсрочкой
* Можно посылать через очередь (есть трейт для извещений)
* Для различных каналов есть свои доп функции. Обычно связанные с оформлением
* Можно добавить поддержку своего канала



## Доп. возможности Mail



Чтобы оформлять письма необязательно создавать представление

```php
public function toMail($notifiable)
{
    $url = url('/invoice/'.$this->invoice->id);

    return (new MailMessage)
                ->greeting('Hello!')
                ->line('One of your invoices has been paid!')
                ->action('View Invoice', $url)
                ->line('Thank you for using our application!');
}
```

Можно использовать шаблоны martdown

```php
public function toMail($notifiable)
{
    $url = url('/invoice/'.$this->invoice->id);

    return (new MailMessage)
                ->subject('Invoice Paid')
                ->markdown('mail.invoice.paid', ['url' => $url]);
}
```

## Ссылки

[Документация Laravel 5.5](https://laravel.com/docs/5.5/notifications)
