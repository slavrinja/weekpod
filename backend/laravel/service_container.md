# Laravel Service Container

[#Home](../.././README.md) | [BackEnd](../README.md)  | [Laravel](laravel.md) 

[TOC]

## Назначение

Чтобы получше понять предыдущую с тему с Service Providers необходимо познакомиться с Контейнером сервисов (Service Container).

Service Container это мощный инструмент для управлениями зависмостями классов и внедрением зависимостей (Dependency injection). DI - это модный термин, означающий внедрение к класс зависимостей через конструктр или методы сеттеры (setters).



## Пример



```php
<?php

namespace App\Http\Controllers;

use App\User;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->users->find($id);

        return view('user.profile', ['user' => $user]);
    }
}
```

В примере UserController требуется, чтобы ему был передан истиочник данных для получения пользователей. За это дело у нас отвечает UserRepository. При иннциализации UserController создатся UserRepository.  За счет того, что у нас описан тип переменной в контроллере будет подгружатся сервис UserRepository. А с чем он будет связан чаще всего определяется в Service  Providers. 

Нам удобно, что мы можем подменять привязку. Например мы можем туда привязать mock или просто поменять реализацию на иную, чем просто выборка из БД.

## Как происходит привязка

Обычно приязки делаются в Service Providers

*В сервис-провайдере всегда есть доступ к контейнеру через свойство `$this->app`*

*Зарегистрировать привязку можно методом `bind()`, передав имя того класса или интерфейса, который мы хотим зарегистрировать*.

*Если классы не зависят от каких-либо интерфейсов, то нет необходимости связывать их в контейнере. Не нужно объяснять контейнеру, как создавать эти объекты, поскольку он автоматически извлекает такие объекты при помощи отражения (reflection).*



## Пример привязки

Пример метода register из Service Provider

```php
use App\Repositories\Backend\History\HistoryContract;
use App\Repositories\Backend\History\EloquentHistoryRepository;
...
public function register()
{
	$this->app->bind(HistoryContract::class, EloquentHistoryRepository::class);
}
...
```

Здесь мы привязаваем к `App\Repositories\Backend\History\HistoryContract` реализацию `App\Repositories\Backend\History\EloquentHistoryRepository`. И везде где у нас указывается HistoryContract будет подключаться класс EloquentHistoryRepository.

Конечно привязка может быть более сложной:

```php
$this->app->bind('HelpSpot\API', function ($app) {
    return new HelpSpot\API($app->make('HttpClient'));
});
```

Тут привязывая мы передаем в объект HelpSpot\API необходмый ему HttpClient

Есть возможность для привязки использовать паттерн sigleton

```php
$this->app->singleton('HelpSpot\API', function ($app) {
    return new HelpSpot\API($app->make('HttpClient'));
});
```

Можно привязывать уже готовые экземпляры

```php
$api = new HelpSpot\API(new HttpClient);
$this->app->instance('HelpSpot\API', $api);
```

И даже обычные переменные

```php
$this->app->when('App\Http\Controllers\UserController')
          ->needs('$variableName')
          ->give($value);
```

В последнем примере если в контроллере имспользуется переменная с именем $variableName то она будет создаваться с указанным значением.

Можно определять разные привязки для разных мест приложения:

```php
$this->app->when(PhotoController::class)
          ->needs(Filesystem::class)
          ->give(function () {
              return Storage::disk('local');
          });

$this->app->when(VideoController::class)
          ->needs(Filesystem::class)
          ->give(function () {
              return Storage::disk('s3');
          });
```



## Получение сервисов (resolving)

Как уже обсуждалось ранее, resolving нужного сервиса их контейнера может происходить когда в констуркторе какого-либо класса указано (type-hint), в качестве типа, имя, к которому была осуществлена привязка. С помощью этого способа мы может внедрять зависимости, например в [controllers](https://laravel.com/docs/5.5/controllers), [event listeners](https://laravel.com/docs/5.5/events), [queue jobs](https://laravel.com/docs/5.5/queues), [middleware](https://laravel.com/docs/5.5/middleware).

Но есть и другие способы.

Например, метод make() контейнера 

```php
$api = $this->app->make('HelpSpot\API');
```

Глобальный хелпер

```php
$api = resolve('HelpSpot\API');
```



## События

Так же resolving имеет события 

```
$this->app->resolving(function ($object, $app) {
  // Вызывается при извлечении объекта любого типа...
});

$this->app->resolving(HelpSpot\API::class, function ($api, $app) {
  // Вызывается при извлечении объекта типа "HelpSpot\API"...
});
```



## Ссылки

[Документация Laravel 5.5](https://laravel.com/docs/5.5/container)
