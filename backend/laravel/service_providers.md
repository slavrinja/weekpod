# Laravel Service Providers

[#Home](../.././README.md) | [BackEnd](../README.md)  | [Laravel](laravel.md) 

[TOC]

## Назначение

Service providers (Поставщики Сервисов) - это центральное место загрузки всего Laravel приложения. Приложение и основные Сервисы загружаются через Service providers.

Здесь регистрируются привязки для Service Container (Контейнера сервисов), event listeners (обработчики событий), middleware и даже маршруты.

Также здесь происходит основная конфигурация вашего приложения.

## Как объявляется и реализуется

Список Поставщиков сервисов находится в config/app.php в массиве providers.

```php
...
/*
 * Application Service Providers...
 */
App\Providers\AccessServiceProvider::class,
App\Providers\AppServiceProvider::class,
App\Providers\AuthServiceProvider::class,
App\Providers\BladeServiceProvider::class,
...
```



Это список, который загружается для вашего приложения. Но многие из них являются отложенными. Они не загружаются при каждом запросе, а только при необходимости. Для таких Service Providers есть свойство `$defer=TRUE`

Вот так может выглядеть внутри Поставщик сервисов

```php
use Riak\Connection;
use Illuminate\Support\ServiceProvider;

class RiakServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Connection::class, function ($app) {
            return new Connection(config('riak'));
        });
    }
}
```

У него есть метод `register()`, который выполянесят при загрузке приложения. В данном случае он производит регистрацию класса в Контейнере сервисов.

При загрузке Laravel приложения берется список Service providers из `config/app.php` и вызывается метод register для каждого из них.

Еще у SP есть метод boot()

```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

/**
 * Load user's messages
 * @package App\Providers
 */
class MessagesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('*', function($view)  {

            $currentUser = access()->user();
            if(empty($currentUser)) {
                $view->with('unreadMessages', []);
                $view->with('importantMessages', []);
                return;
            }
            $result = access()->user()->notifications;
            $unread = $important = [];
            foreach($result as $row) {
                if(empty($row->read_at)) {
                    $message = [ 'title' => $row->data['title'], 'id' => $row->data['message_id'] ];
                    $unread[] = $message;
                    if($row->data['important']) {
                        $important[] = $message;
                    }
                }
            }

            // pass the data to the view
            $view->with('unreadMessages', $unread);
            $view->with('importantMessages', $important);
        });
    }
}
```

Который вызывается, когда register() всех SP вызываны. В методе boot уже можно обращатсья к результатам работы любого SP.

В данном примере между всеми представлениями приложения расшариваются переменные содержащие массивы сообщений пользователям.

## Отложенные Service Providers

Пример отложеного SP:

```php
<?php

namespace App\Providers;

use Riak\Connection;
use Illuminate\Support\ServiceProvider;

class RiakServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Connection::class, function ($app) {
            return new Connection($app['config']['riak']);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Connection::class];
    }

}
```



В данном примере в Контейнере сервисов регистрируется класс Riak\Connection. Но register() не будет выполнен до тех пор пока мы не запросим сервис Riak\Connection где-либо в нашем приложении.



## Ссылки

[Документация Laravel 5.5](https://laravel.com/docs/5.5/providers)

[Доментация Laravel 5.3 на русском](https://laravel.ru/posts/253)

