# Frontend

[#Home](.././README.md)

## Разделы

* [Разное](frontend_misc.md)
* [Google Analytics](analytics.md)
* [Chrome Apps](chrome_apps.md)
* [NodeJs](node.md)
* [Code snippets. Разное](frontend_misc.md)
* [Инструменты](tools.md)
* [VueJS](vue.md)
* [Web component](web_component.md)
* [Web workers](web_workers.md)
* [Webassembly](webassembly.md)
* [Websockets](websockets.md)


## Полезные материалы

* [30 seconds of code](https://30secondsofcode.org/) - Curated collection of useful JavaScript snippets that you can understand in 30 seconds or less.
* [Объясняем современный JavaScript динозавру]( https://habrahabr.ru/company/mailru/blog/340922)
* [От нуля до героя фронтенда 1](https://medium.com/russian/%D0%BE%D1%82-%D0%BD%D1%83%D0%BB%D1%8F-%D0%B4%D0%BE-%D0%B3%D0%B5%D1%80%D0%BE%D1%8F-front-enda-%D1%87%D0%B0%D1%81%D1%82%D1%8C-1-f524d668f328) 
* [От нуля до героя фронтенда 2](https://medium.com/russian/%D0%BE%D1%82-%D0%BD%D1%83%D0%BB%D1%8F-%D0%B4%D0%BE-%D0%B3%D0%B5%D1%80%D0%BE%D1%8F-%D1%84%D1%80%D0%BE%D0%BD%D1%82%D0%B5%D0%BD%D0%B4%D0%B0-%D1%87%D0%B0%D1%81%D1%82%D1%8C-2-25f19e56eb29) 
* [Front-End Developer Handbook 2017](https://legacy.gitbook.com/book/frontendmasters/front-end-handbook-2017/details)
* [Front-End Developer Handbook 2018](https://legacy.gitbook.com/book/frontendmasters/front-end-developer-handbook-2018/details)
* [Современный CSS для динозавров](https://habrahabr.ru/post/348500/)
* [Объясняем современный JavaScript динозавру](https://m.habrahabr.ru/company/mailru/blog/340922/)
* [Top 10 JavaScript errors from 1000+ projects (and how to avoid them)](https://rollbar.com/blog/top-10-javascript-errors/)
* [11 библиотек (наборов компонентов) для Vue, о которых стоит знать в 2018-м](https://m.habrahabr.ru/company/ruvds/blog/346220/)
* [Принципы работы современных веб-браузеров](https://www.html5rocks.com/ru/tutorials/internals/howbrowserswork/#The_main_flow)
* [16 Tips that Will Improve Any Online Form](https://uxplanet.org/the-18-must-do-principles-in-the-form-design-fe89d0127c92)
* [статья о полььзе анимации](https://habrahabr.ru/company/ruvds/blog/321822/) 
* [Хорошая большая статья о типографике](https://habrahabr.ru/company/mailru/blog/344132/)
* [удобная дока по элементам HEAD](https://gethead.info/)
* Тренды веб-дизайна в 2018 году
  - http://telegraf.design/trendy-veb-dizajna-v-2018-godu/
  - https://trends.uxdesign.cc/
* https://learn.javascript.ru/webcomponents
* https://threejs.org/
* http://kamranahmed.info/driver
*[A Beginner’s Guide to npm — the Node Package Manager](https://www.sitepoint.com/beginners-guide-node-package-manager/)
* https://github.com/yangshun/front-end-interview-handbook
* [ES-2015 сейчас](https://learn.javascript.ru/es-modern-usage)
* https://habrahabr.ru/post/303626/
* https://medium.com/@serhii.matrunchyk/using-laravel-localization-with-javascript-and-vuejs-23064d0c210e
* https://www.youtube.com/watch?v=kZvTUNGrHrw
* [JS библиотека moment](https://momentjs.com/) для операций с датами и форматирования дат.
* [GitHub: обфускатор js](https://github.com/javascript-obfuscator/javascript-obfuscator). Есть много опций для управления обфускацией. Есть [онлайн-версия](https://javascriptobfuscator.herokuapp.com/). Можно запускать из консоли. Есть плагины для WebPack, Grunt и Gulp.
* https://cdn-images-1.medium.com/max/1200/1*YXS6pEeuEXEjTpzrrLiCmA.jpeg
* http://adit.io/posts/2013-04-17-functors,_applicatives,_and_monads_in_pictures.html
* adit.io
* https://i0.wp.com/blog.mallow-tech.com/wp-content/uploads/2016/06/Laravel-Request-Life-Cycle.png?resize=1024%2C559

