# Code snippets. Разное

[#Home](.././README.md) | [FrontEnd](README.md)

## Обмен сообщениями с iframe

### iframe на родном домене

Внутри 	IFRAME

```javascript
var event = new CustomEvent('i_event', { "detail": eName});
var b = document.getElementById('body');
b.dispatchEvent(event);
```

На странице родителя

```javascript
$('#frame').load(function(){
	$(this).contents().find("body").on('i_event', function(event) {
		var eType = event.originalEvent.detail;
        console.log('got i_event with detail:' + eType);
    });
});
```



### iframe на другом домене (при условии что мы имеем к нему доступ)

Внутри 	IFRAME

```javascript
window.parent.postMessage({
	'event': 'i_event',
	'location': window.location.href
}, "*");
```

На странице родителя

```javascript
window.addEventListener('message', function (event) {
    if (event.data.event == "i_event") {        
        console.log('got i_event');            
    }
}, false);
```


## Возврат к дефолтному событию после e.preventDefault();

```javascript
$('.confirm-submit').click(function(e) {
    e.preventDefault();
    var that = this;
    var href = $(this).attr('href');    
    
  if (typeof href !== undefined && href) {
      window.location = '/'+href;// link action
    } else {
      $(that).unbind('click').click(); // button action
    }
    return false;
});
```

## БЭМ
- https://ru.bem.info
- https://htmlacademy.ru/shorts/5


## подсветка как в руководстве

* http://thetheme.io/theadmin/extension/tour.html
* http://github.hubspot.com/shepherd/

## Гриды

- Серия коротких статей С КАРТИНКАМИ чтобы познакомиться с гридами и перестать их бояться
- Учим CSS Grid за 5 минут
  - https://m.habrahabr.ru/company/edison/blog/343614/
- Как быстро спроектировать сайт с помощью CSS Grid
  - https://m.habrahabr.ru/company/edison/blog/343796/Linux
- Делаем адаптивный HTML, добавляя одну строку в CSS
  - https://habrahabr.ru/company/edison/blog/344878/
 
## web Vs. Android

- [Android уже при смерти. Ищите пути отступления](https://life.ru/t/%D0%B3%D0%B0%D0%B4%D0%B6%D0%B5%D1%82%D1%8B/1082482/android_uzhie_pri_smierti_ishchitie_puti_otstuplieniia)
- [Будущее мобайла — не приложения, а браузеры](https://habrahabr.ru/company/alconost/blog/320250/)


## flexbox
https://caniuse.com/#search=flexbox
https://itelmenko.ru/wiki/css/flexbox/
https://aerolab.co/blog/flexbox-grids/
