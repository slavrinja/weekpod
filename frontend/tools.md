# Frontend. Библиотеки, инструменты

[#Home](.././README.md) | [FrontEnd](README.md)

[TOC]

## Parceljs - быстрый. но пока сыроваты ИМХО аналог вебпак

- https://ru.parceljs.org/ 
- https://habrahabr.ru/post/344486/
- елси верить главной, работает быстрее webpack
- Перед нами эдакий "Webpack на минималках", предоставляющий возможность быстрого развертывания рабочего окружения для небольшого проекта. Стек технологий по сути ограничен лишь стандартным набором ассетов, но в любой момент его можно расширить и своими собственными. С учетом полной поддержки Babel мы легко можем использовать практически любой другой фреймворк или библиотеку (разве что с Angular будут сложности, ведь писать с его помощью на ES6 и без родного инструментария — задача на любителя), а поддержка PostCSS из коробки является еще одним приятным дополнением.
- вариант для небольшого проекта
- компонентный подход
- мало доки и примеров

## Bourbon - библиотека миксинов для Sass

- http://bourbon.io/
- легковесный аналог Compass
- набор наиболее частор используемых миксинов - анимации, скругления, бэкграунды, плейсхолдеры и прочее


### Быстрая настройка stylelint для Intellij IDEA

1. идём в настройки IDE (Ctrl+Alt+S) File | Settings и находим Node.js and NPM в разделе  Languages & Frameworks
2. видим список пакетов. справа нажимаем + и поиском по "stylelint" находим и ставим 2 пакета stylelint и stylelint-config-standard  (это стандарный набор правил, их потом можно расширить)
3. в настройках линтера ставим галку Enable а так же указываем путь к установленному пакету
4. в корень проекта добавляем файл конфига .stylelintrc в котором прописаны правила проверки
5. непосредственно открыв файл можно запустить линтинг Alt+Enterr

- https://stylelint.io/user-guide/configuration/
- <https://habrahabr.ru/post/301594/>
- <https://habrahabr.ru/post/301594/>
- https://www.jetbrains.com/help/idea/stylelint.html
- https://www.npmjs.com/package/stylelint-scss
- https://www.npmjs.com/package/stylelint-
scss



## lint

ставим в проекте

- npm i stylelint-config-standard stylelint-scss stylelint --save-dev

создать в корне проекта файл .stylelintrc

```
{
  "extends": [
    "stylelint-config-standard"
  ],
  "plugins": [
    "stylelint-scss"
  ],
  "rules": {
    "comment-empty-line-before": true,
    "no-invalid-double-slash-comments": false,
    "value-list-comma-space-after": [2, "always"],
    "rule-empty-line-before": true,
    "at-rule-empty-line-before": null,
    "scss/dollar-variable-pattern": "^foo",
    "scss/selector-no-redundant-nesting-selector": true
  }
}
```

в IDE

- идём в настройки IDE (Ctrl+Alt+S) File | Settings и находим Node.js and NPM в разделе  Languages & Frameworks
- видим список пакетов. справа нажимаем + и поиском по "stylelint" находим и ставим 3 пакета stylelint и stylelint-config-standard stylelint-scss  (это стандарный набор правил, их потом можно расширить)
- в настройках Stylelint (Languages & Frameworks > stylesheets > Stylelint)ставим галку Enable а так же указываем путь к установленному пакету (~/www/vidoza.local/front/www/htdocs/node_modules/stylelint)
- внутри scss файла можно запустить прогон линтера через Alt+Enter

