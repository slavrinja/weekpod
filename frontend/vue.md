# Vue.js

[#Home](.././README.md) | [FrontEnd](README.md)

[TOC]

## Полезное 
* [Vue-bootstrap](https://bootstrap-vue.js.org/) связка Vue и Bootstrap 4
* [Видео урок Vue-cli](https://www.youtube.com/watch?v=kZvTUNGrHrw)
* [Видео урок Vue-router](https://www.youtube.com/watch?v=Hv5SM19oQ6w)
* [Видео урок Vue-resource](https://www.youtube.com/watch?v=YZEm4dTnP80)
* [Видео урок Vuex](https://www.youtube.com/watch?v=uiavUaBXxXw)

## Nuxt.js
* [Nuxt.js: Документация](https://ru.nuxtjs.org/)
* [Nuxt.js: Фреймворк для фреймворка Vue.js](https://habrahabr.ru/post/336902/)

## Локализация Javascript

- [M: Using Laravel localization with JavaScript and VueJS](https://medium.com/@serhii.matrunchyk/using-laravel-localization-with-javascript-and-vuejs-23064d0c210e)
- Компонент [Vue-i18n](http://kazupon.github.io/vue-i18n/en/sfc.html)

## Тестирование

- [Виды тестирования](http://wiki.rosalab.ru/ru/index.php/%D0%92%D0%B8%D0%B4%D1%8B_%D1%82%D0%B5%D1%81%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F_%D0%9F%D0%9E)
- [How to Write A Unit Test for Vue.js ― Scotch](https://www.instapaper.com/read/986903107) Karma, Mocha, Chai, avoriaz; vue-cli
- [TDD with Vue.js](https://blog.octo.com/en/tdd-with-vue-js/)
- [Testing a Vue component. 5 parts](https://www.instapaper.com/read/986903151) Jest
- [Integrating Unite.js with Laravel Projects for TDD of Vue.js Components](https://codeburst.io/integrating-unite-js-with-laravel-projects-701b53d94626)

## Full-stack на Vue.js, Express.js и MongoDB

Заинтересовала легкая статья на медиуме [Создание MEVN-приложения](https://medium.com/devschacht/%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-mevn-%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F-%D1%87%D0%B0%D1%81%D1%82%D1%8C-1-2-9ad714260037). 

До начала ее чтения полезно  (и возможно необходимо) ознакомиться со следующими вещами:

* [npm](https://itelmenko.ru/wiki/npm/) - менеджер пакетов для node.js
* [yarn](https://itelmenko.ru/wiki/yarn/) - обертка для npm которая добавляет удобства
* [nvm](https://itelmenko.ru/tutorials/javascript-tutorials/install-nodejs-emberjs/) - менеджер версий npm
* [vue-cli](https://itelmenko.ru/wiki/javascript/js-frameworks/vue-cli/) - консольная утилита для создания каркаса приложения **vue.js** на основе одного из имеющихся шаблонов
* [Стрелочные функции JS](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Functions/Arrow_functions) - более короткая запись для анонимных функций с решением пробемы потери контекста this
* [Промисы в js](https://itelmenko.ru/wiki/promise) -  более удобный механизм организации асинхронного кода, чем колбеки. Призваны убрать проблему callback hell, когда код имеет бешеную сложенность.
* async/await - более удобрый способ организации асинхронного кода чем промисы. Полезны статьи [6 причин забыть о промисах](https://habrahabr.ru/company/ruvds/blog/326074/) и [Async/await за 10 минут](https://golos.io/javascript/@shass/javascript-async-await-za-10-minut).  `async/await` это часть ECMAScript 2017 и не поддерживается IE и старыми браузерами. [caiuse](https://caniuse.com/#feat=async-functions). Примеры эволюции оформления асинхронного кода в статье [Callbacks, Promises and Async/Await](https://medium.com/front-end-hacking/callbacks-promises-and-async-await-ad4756e01d90)
* [axios](https://github.com/axios/axios) - Основанный на промисах http-клиент для браузерного кода и node.js. Может использоваться и с async/await.

И вещи, которые будут использоваться в статье, но знание этих вещей не требуется, так как по статье будет понятно:

* [vue-router](https://router.vuejs.org/ru/) - создание маршрутов в vue-приложении
* [express.js](http://expressjs.com/)  - минималистичный web-фреймворк для node.js
* [mongodb](https://ru.wikipedia.org/wiki/MongoDB) - (СУБД) с открытым исходным кодом, не требующая описания схемы таблиц. MongoDB — это документарная база данных. Вместо хранения данных в **таблицах**, состоящих из отдельных **строк**, как в реляционных базах, MongoDB сохраняет данные в **коллекциях**, состоящих из **документов**. Документ — это большой JSON объект **без** заранее определенного формата и схемы.

После чтения статьи уже можно более полно окунуться в эти вещи.

* [Современные возможности ES-2015](https://learn.javascript.ru/es-modern)
* [Почему вы никогда не должны использовать MongoDB](https://habrahabr.ru/post/231213/)
* [MySQL и MongoDB — когда и что лучше использовать](https://habrahabr.ru/post/322532/)