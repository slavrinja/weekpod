# Web Compomnents

[#Home](.././README.md) | [FrontEnd](README.md)

[TOC]

Веб-компоненты включают четыре технологии, каждая из которых может использоваться отдельно от других:

- Custom Elements — API для создания собственных HTML элементов.
- HTML Templates — тег <template> позволяет реализовывать изолированные DOM-элементы.
- Shadow DOM — изолирует DOM и стили в разных элементах.
- HTML Imports — импорт HTML документов.

  
```js
document.registerElement(имя, { prototype: прототип })
```
  
```html
<script>
  // прототип с методами для нового элемента
  var MyTimerProto = Object.create(HTMLElement.prototype);
  MyTimerProto.tick = function() { // свой метод tick
    this.innerHTML++;
  };

  // регистрируем новый элемент в браузере
  document.registerElement("my-timer", {
    prototype: MyTimerProto
  });
</script>

<!-- теперь используем новый элемент -->
<my-timer id="timer">0</my-timer>

<script>
  // вызовем метод tick() на элементе
  setInterval(function() {
    timer.tick();
  }, 1000);
</script>
```
  
```html
<script>
  var MyTimerProto = Object.create(HTMLButtonElement.prototype);
  MyTimerProto.tick = function() {
    this.innerHTML++;
  };

  document.registerElement("my-timer", {
    prototype: MyTimerProto,
    extends: 'button'
  });
</script>

<button is="my-timer" id="timer">0</button>

<script>
  setInterval(function() {
    timer.tick();
  }, 1000);

  timer.onclick = function() {
    alert("Текущее значение: " + this.innerHTML);
  };
</script>
```

### <template>

```html
<table id="producttable">
  <thead>
    <tr>
      <td>UPC_Code</td>
      <td>Product_Name</td>
    </tr>
  </thead>
  <tbody>
    <!-- existing data could optionally be included here -->
  </tbody>
</table>

<template id="productrow">
  <tr>
    <td class="record"></td>
    <td></td>
  </tr>
</template>
```

```js
// Получаем объект шаблона
var t = document.querySelector('#productrow'),
   
// Меняем содержимое ячеек в шаблоне    
td = t.content.querySelectorAll("td");
td[0].textContent = "1235646565";
td[1].textContent = "Stuff";

// Клонируем новую строку и вставляем её в таблицу
var tb = document.getElementsByTagName("tbody");
var clone = document.importNode(t.content, true);
tb[0].appendChild(clone);
  
// Опять меняем содержимое ячеек, чтобы получилась новая строка
td[0].textContent = "0384928528";
td[1].textContent = "Acme Kidney Beans";

// Клонируем новую строку и вставляем её в таблицу
var clone2 = document.importNode(t.content, true);
tb[0].appendChild(clone2);
```

* [Shadow Dom](https://learn.javascript.ru/article/shadow-dom/shadow-dom-chrome.png)
 
```html
<p id="elem">Доброе утро, страна!</p>

<script>
  var root = elem.createShadowRoot();
  root.innerHTML = "<h3><content></content></h3> <p>Привет из подполья!</p>";
</script>
```

```html
<h3>Доброе утро, страна!</h3>
<p>Привет из подполья!</p>
```
```html
<p id="elem">
<h3>
  <content></content>
</h3>
<p>Привет из подполья!</p>";
  "Доброе утро, страна!"
</p>
```
```
<content select="h3"></content>
```
```
<link rel="import">
```
```html
<!DOCTYPE HTML>
<html>

<body>

  <script>
    function show() {
      var time = link.import.querySelector('time')
      document.body.appendChild(time);
    };
  </script>

  <link rel="import" id="link" onload="show()" href="timer.html">

</body>

</html>
```

### timer.html
```html
<!DOCTYPE HTML>
<html>

<body>

  <time id="timer">0</time>

  <script>
    var localDocument = document.currentScript.ownerDocument;
    var timer = localDocument.getElementById('timer');

    var timerId = setInterval(function() {
      timer.innerHTML++;
    }, 1000);
  </script>

</body>
```

### 3 различных продукта для создания PWA и моб приложений на Vue.js

- [Quasar Framework](http://quasar-framework.org) - Build responsive websites, PWAs, hybrid mobile Apps (through Cordova) and Desktop apps (through Electron), all simultaneously using same codebase
    - http://quasar-framework.org/components
    - [Quasar Framework — универсальный Vue-фреймворк
](https://medium.com/devschacht/quasar-framework-%D1%83%D0%BD%D0%B8%D0%B2%D0%B5%D1%80%D1%81%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9-vue-%D1%84%D1%80%D0%B5%D0%B9%D0%BC%D0%B2%D0%BE%D1%80%D0%BA-d95c0350efd4)
- [NativeScript-Vue](https://nativescript-vue.org/)
- [Weex](https://weex.incubator.apache.org) - a framework for building Mobile cross-platform UIs. Different from a "web app", "HTML5 app", or "hybrid app", you can use Weex to build a real mobile app. The code that you write is relatively simple,

- [Native apps with Vue.js: Weex or NativeScript?](https://hackernoon.com/native-apps-with-vue-js-weex-or-nativescript-8d8f0bac041d)
