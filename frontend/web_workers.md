# Web Workers API

[#Home](.././README.md) | [FrontEnd](README.md)

[TOC]

###План

* Для чего может служить, что умеет
* Как создается
* Как взаимодействует с остальным кодом страницы
* Какие ограничения
* Разделяемые воркеры (порты)
* Серилизация при передаче (Transferable, Uint8Array)
* Другие типы воркеров
  * Service Workers
  * Chrome Workers
  * Auto Workers
  * Встроеные
* Blob, File, BlogBuilder (устарел), URL.createObjectURL



## Назначение и возможности

[Доступность в браузерах по данным CanIUse](https://caniuse.com/#feat=webworkers,sharedworkers)

Web Workers API служит для запуска скриптов в фоне. Если нам необходимо запустить в фоне какие-либо вычисления, например, да еще и в несколько потоков, то это подходящий инструмент для таких целей. Так как работа происходит в фоне, то при этих вычислениях рендеринг страницы не блокируется.  Всем происходит параллельно.

С создавшим его кодом каждый Web Worker общаеся через отправку событий.

Есть разные типы вокеров. простейшим является выделенный воркер.

## Создание и взаимодействие

Создание выделенного воркера происходит просто:

```js
if (window.Worker) {
	// Создание воркера
	var myWorker = new Worker("worker.js");
  	// Обработчик событий (получение данных) от воркера
    myWorker.onmessage = function(e) {
    	console.log('Message received from worker', e.data);
    }
    // Отправка данных воркеру
    myWorker.postMessage([34, 10]);
}  
```

Здесь проверяется доступность Web Wokres API и создается воркер из файла worker.js, а также добавляется обработка событий от воркера.

Код worker.js может сорержать функцию `onmessage` для приема данных от основного js кода:

```js
onmessage = function(e) {
  // Получение данных от основного кода
  console.log('Message received from main script', e.data);
  var workerResult = e.data[0]*e.data[1];
  console.log('Posting message back to main script');
  // Отправка результата в основной код
  postMessage(workerResult);
}
```



Если первый код вставить в index.html между тегами script, а второй кусок кода поместить в wokrer.js, то запустив index.html мы получим в консоли браузера сообщения:

```
Message received from main script (2) [34, 10]
Posting message back to main script
Message received from worker 340
```

У MDN есть пример [simple-web-worker](https://github.com/mdn/simple-web-worker). И [demo](http://mdn.github.io/simple-web-worker). Там код лишь немного сложнее. Показаны 2 воркера.

Worker-ы могут запускать другие worker-ы. Главное чтобы политика одного источника соблюдалась.

Также из воркетов имеется доступ к функции `importScripts('foo.js', 'bar.js' ...);`   для загрузки  скриптов извне. Загрузка должна быть только с того же домена.

## Ограничения

В воркере нельзя обращаться к DOM. Нет доступа к localStorage. Эти вещи можно делать только передав событие основному коду, который сделает все что нужно.

## Разделяемые воркеры

Разделяемый воркер доступен нескольким разным скриптам — даже если они находятся в разных окнах, фреймах или даже воркерах.

Создается такой воркер похожим образом:

```js
var myWorker = new SharedWorker("worker.js");
```

Как видно, в данном случае используется конструктор SharedWorker.

С разделяемым worker-ом необходимо взаимодействовать через объект `port` — явно открыв порт, с помощью которого скрипты могут взаимодействовать с worker-ом. Для выделенного worker-а это происходит тоже, но неявно (через `onmessage`).

Соединение с портом должно быть осуществлено либо неявно, используя обработчик событие `onmessage`, либо явно, вызвав метод `start()` перед тем, как отправлять любые сообщения. 

Например, в родительском потоке можно 

```js
// Открываем порт
myWorker.port.start();
// Передаем данные
myWorker.port.postMessage(['value1', 'value2', 'value3']);
```

При этом на стороне воркера:

```js
// Необходим addEventListener() для события onconnect
self.addEventListener('connect', function(e) {
  var port = e.ports[0]; // Получаем первый порт
  port.onmessage = function(e) {
    var workerResult = 'Result: ' + (e.data[0] + e.data[1] + e.data[2]);
    port.postMessage(workerResult);
  }
});
```

В родительском потоке:

```js
myWorker.port.onmessage = function(e) {
  console.log('Message received from worker', e.data[0]);
}
```

Таким образом, если раньше у нас был 1 обработчик данных в воркере, то теперь их может быть много с разным функционалом. Что может быть использовано для связи с различными скриптами.

## Передача данных в воркеры и из них

Данные в воркеры и из них передаются копированием при помощи сериализации. По ссылке данные не передаются. Однако у Chrome 17+ и Firefox 18+  имеется возможность передать некоторые типы объектов по ссылке. Это объекты, которые реализуют  интерфейс [Transferable](https://developer.mozilla.org/ru/docs/Web/API/Transferable). Пока что это только ArrayBuffer и MessagePort.

Чтобы передать таким образом парааметр необходимо воспользоваться вторым аргуметом метода `postMessage`:

```js
var ab = new ArrayBuffer(size);
worker.postMessage({ data: ab }, [ab]);
```

## Другие типы воркеров

Имеются развноидности воркеров. 

* [ServiceWorkers](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API) - служат для реализации работы приложений в условиях потери связи. С помощью них можно реализовать выдачу закешированного результата. А также push-уведомления, и фоновую синхронизацию. [caniuse](https://caniuse.com/#feat=serviceworkers)
* [Chrome Workers](https://developer.mozilla.org/ru/docs/Web/API/ChromeWorker) - это воркеры которые используются при разработке расширений к браузеру Firefox.
* [Audio Workers](https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API#Audio_Workers) для работы с audio потоком из воркеров. Но не рализованы пока в браузерах
* Встроеные воркеры



## Интересные объекты, которые попались по пути

URL.createObjectURL, Blob, BlogBuilder (устарел), File

[URL.createObjectURL](https://developer.mozilla.org/ru/docs/Web/API/URL/createObjectURL) - статический метод, который создает [`DOMString`](https://developer.mozilla.org/ru/docs/Web/API/DOMString), содержащий URL с указанием на объект, заданный как параметр. Новый URL объект может представлять собой [`File`](https://developer.mozilla.org/ru/docs/Web/API/File) объект или [`Blob`](https://developer.mozilla.org/ru/docs/Web/API/Blob) объект.

Например, создание воркера без внешнего файла:

```js
var response = "self.onmessage=function(e){postMessage('Worker: '+e.data);}";

var blob;
try {
    blob = new Blob([response], {type: 'application/javascript'});
} catch (e) { // Backwards-compatibility
    window.BlobBuilder = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder;
    blob = new BlobBuilder();
    blob.append(response);
    blob = blob.getBlob();
}
var worker = new Worker(URL.createObjectURL(blob));

// Test, used in all examples:
worker.onmessage = function(e) {
    alert('Response: ' + e.data);
};
worker.postMessage('Test');
```



Так же можно поступать с любыми mime типами. Картинка

```js

  var base64 = 
    "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB1klEQVR42n2TzytEURTHv3e8N1joRhZG" + 
    "zJsoCjsLhcw0jClKWbHwY2GnLGUlIfIP2IjyY2djZTHSMJNQSilFNkz24z0/Ms2MrnvfvMu8mcfZvPvu" + 
    "Pfdzz/mecwgKLNYKb0cFEgXbRvwV2s2HuWazCbzKA5LvNecDXayBjv9NL7tEpSNgbYzQ5kZmAlSXgsGG" + 
    "XmS+MjhKxDHgC+quyaPKQtoPYMQPOh5U9H6tBxF+Icy/aolqAqLP5wjWd5r/Ip3YXVILrF4ZRYAxDhCO" + 
    "J/yCwiMI+/xgjOEzmzIhAio04GeGayIXjQ0wGoAuQ5cmIjh8jNo0GF78QwNhpyvV1O9tdxSSR6PLl51F" + 
    "nIK3uQ4JJQME4sCxCIRxQbMwPNSjqaobsfskm9l4Ky6jvCzWEnDKU1ayQPe5BbN64vYJ2vwO7CIeLIi3" + 
    "ciYAoby0M4oNYBrXgdgAbC/MhGCRhyhCZwrcEz1Ib3KKO7f+2I4iFvoVmIxHigGiZHhPIb0bL1bQApFS" + 
    "9U/AC0ulSXrrhMotka/lQy0Ic08FDeIiAmDvA2HX01W05TopS2j2/H4T6FBVbj4YgV5+AecyLk+Ctvms" + 
    "QWK8WZZ+Hdf7QGu7fobMuZHyq1DoJLvUqQrfM966EU/qYGwAAAAASUVORK5CYII=";

  var binary = fixBinary(atob(base64));
  var blob = new Blob([binary], {type: 'image/png'});
  var url = URL.createObjectURL(blob);
  log('Created a png blob of size: ' + blob.size);
  log('Inserting an img...');
  var img = document.createElement('img');
  img.src = url;
  document.body.appendChild(img);
```

[Полный пример с картинкой](https://gist.github.com/nolanlawson/0eac306e4dac2114c752)

[Blob](https://developer.mozilla.org/ru/docs/Web/API/Blob) объект представляет из себя объект наподобие файла с неизменяемыми, сырыми данными. Blob-ы представляют данные, которые могут быть не в родном формате JavaScript. Интерфейс [`File`](https://developer.mozilla.org/ru/docs/Web/API/File) основан на `Blob`, наследует функциональность `Blob` и расширяет его для поддержки файлов на системе пользователя.

```js
var debug = {hello: "world"};
var blob = new Blob([JSON.stringify(debug, null, 2)], {type : 'application/json'});
```

До того как конструктор Blob стал доступен, это могло быть выполнено через устаревший API [`BlobBuilder`](https://developer.mozilla.org/ru/docs/Web/API/BlobBuilder):

```js
var builder = new BlobBuilder();
var fileParts = ['<a id="a"><b id="b">hey!</b></a>'];
builder.append(fileParts[0]);
var myBlob = builder.getBlob('text/xml');
```



[Встроеные воркеры](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers)  перейти вручную нк разделу Embedded workers

```js
var blob = new Blob(Array.prototype.map.call(document.querySelectorAll('script[type=\'text\/js-worker\']'), function (oScript) { return oScript.textContent; }),{type: 'text/javascript'});

  // Creating a new document.worker property containing all our "text/js-worker" scripts.
document.worker = new Worker(window.URL.createObjectURL(blob));

document.worker.onmessage = function(oEvent) {
  pageLog('Received: ' + oEvent.data);
};
// Start the worker.
window.onload = function() { document.worker.postMessage(''); };
```

Также можно конвертировать функцию в Blob:

```js
function fn2workerURL(fn) {
  var blob = new Blob(['('+fn.toString()+')()'], {type: 'application/javascript'})
  return URL.createObjectURL(blob)
}
```



Интерфейс **File** представляет информацию о файлах и предоставляет JavaScript в веб странице доступ к их контенту.

`Объекты File`  как правило извлекаются из объекта [`FileList`](https://developer.mozilla.org/ru/docs/Web/API/FileList), который возвращён как результат пользовательского выбора файлов с помощью `<input>` элемента, из drag and drop операций [`DataTransfer`](https://developer.mozilla.org/ru/docs/Web/API/DataTransfer) объекта, или из `mozGetAsFile()` API на [`HTMLCanvasElement`](https://developer.mozilla.org/ru/docs/Web/API/HTMLCanvasElement). 
