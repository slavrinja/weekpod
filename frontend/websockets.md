# WebSockets, альтернативы и смежное

[#Home](.././README.md) | [FrontEnd](README.md)

[TOC]


##Альтернативы WebSockets
[Что такое Long-Polling, WebSockets, SSE и Comet](https://m.vk.com/page-76525381_49016199)
* Ajax Pooling - частые AJAX-запросы к серверу, чтобы узнать нет ли данных для клиента
* AJAX Long-Polling - AJAX-запрос висит долго пока не получит ответ (имитирует событие от сервера), после чего запрос завершается, но на его смену приходит другой
* [Server Sent Events ](https://learn.javascript.ru/server-sent-events) похоже на развитие Long-Polling
###Server Sent Events
*Современный стандарт [Server-Sent Events](https://html.spec.whatwg.org/multipage/comms.html#the-eventsource-interface) позволяет браузеру создавать специальный объект `EventSource`, который сам обеспечивает соединение с сервером, делает пересоединение в случае обрыва и генерирует события при поступлении данных.*
При создании объекта `new EventSource(src)` браузер автоматически подключается к адресу `src` и начинает получать с него события:
```js
var eventSource = new EventSource("/events/subscribe");
eventSource.onmessage = function(e) {
  console.log("Пришло сообщение: " + e.data);
};
```
*Чтобы соединение успешно открылось, сервер должен ответить с заголовком `Content-Type: text/event-stream`, а затем оставить соединение висящим и писать в него сообщения в специальном формате:*
```
data: Сообщение 1
data: Сообщение 2
data: Сообщение 3
data: из двух строк
```
Кроме  `onmessage` можно создавать свои имена событий и вешать обработчики через `addEventListener`.
`EventSource` поддерживает кросс-доменные запросы, аналогично `XMLHttpRequest`. Нужен заголовок `Origin`.
Есть возможность указывать идентификатор последнего id события `Last-Event-ID` чтобы получить события, которые были после него. У WebSocket такого нет из коробки.
## Переключение между различными транспортами
Некоторые WebSocket библиотеки умеют переключаться между транспортами WebSocket и Long-pooling. Например, https://socket.io/docs/client-api/
## Смежное
[WebRTC и Websockets. Есть ли разница](http://qaru.site/questions/114679/webrtc-and-websockets-is-there-a-difference)
Пришел на смену Flash, Java и ActiveX
*В WebRTC есть две стороны.* (RTC - real-time communications)
- *API JavaScript (`getUserMedia`), которые позволяют приложению получать доступ к аппаратам камеры и микрофона, а также захватывать изображение рабочего стола. Вы можете использовать этот доступ для простого отображения потока локально (возможно [применения эффектов](http://neave.com/webcam/html5/)) или отправки потока по сети. Вы можете отправить данные на свой сервер или использовать...*
- *`PeerConnection`, API, который позволяет браузерам устанавливать соединения сокета **direct peer-to-peer**. Вы можете напрямую установить соединение с другим браузером и обмениваться данными напрямую. Это очень полезно для данных с высокой пропускной способностью, таких как видео, где вы не хотите, чтобы ваш сервер имел дело с ретрансляцией больших объемов данных.*
  *<u>Для обмена данными между двумя участниками видеосервер не требуется, но если нужно объединить в одной конференции несколько участников, сервер необходим</u>.*
Можно создавать многопользовательский чат
[Многопользовательский чат с использованием WebRTC](https://m.habrahabr.ru/post/255833/)
[WebRTC для звонков из чистого браузера](https://xakep.ru/2014/10/27/web-rtc/)
[Видео-чат через браузер. WebRTC — это просто, если есть библиотека](https://habrahabr.ru/post/312868/)
[Обзорная статья: WebRTC. Видеоконференции в браузере](https://trueconf.ru/webrtc.html)
### Готовые приложения для видео чатов и скрин-шаринга
[Goodbye Slack and hello open-source messaging platform](https://medium.com/santiment/goodbye-slack-hello-open-source-messaging-platform-c97398a20ce9)
Javascript приложение [**Rocket.Chat**](https://github.com/RocketChat/Rocket.Chat)
- Audio calls
- Multi-users Audio Conference
- Screensharing
- (Beta) Face to Face Video Conferencing (aka WebRTC )
- (Beta) Multi-users Video Group Chat
[Муки выбора корпоративного мессенджера: бесплатно, свой сервер, не iChat](https://geektimes.ru/post/291899/)
Легко ставится на сервер и есть клиенты для разных платформ (включая web).
