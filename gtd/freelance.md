# Фриланс

[#Home](.././README.md) | [GTD](README.md)

## Старт работы Upwork

* [Гайд по работе на Апворке](http://odeskconf.github.io/guide/)
* [Пошаговый план старта на Upwork](https://habrahabr.ru/post/337136/)
* [Пошаговый план старта на Upwork #2](https://habrahabr.ru/post/337200/)
* [Легальный вывод средств с Upwork в РФ](https://habrahabr.ru/post/327130/)
* [Russian Freelancer Documentation for Wire Transfers](https://community.upwork.com/t5/Announcements/Russian-Freelancer-Documentation-for-Wire-Transfers/td-p/248545)
* [Купил трусы, а все уже знают](http://www.trud.ru/article/17-12-2016/1345043_kupil_trusy_a_vse_uzhe_znajut.html)

## Upwork подготовка к тестам

* http://upworktestru.com/
* http://upworktestru.com/css_test_2016_upwork_answers_questions/
* http://upworktestru.com/javascript_test_2016_upwork_answers_questions/
