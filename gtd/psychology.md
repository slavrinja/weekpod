# Психология

[#Home](.././README.md) | [GTD](README.md)

## Карьера

* [Работа в офисе пойдет на пользу вашей карьере](https://rb.ru/story/stay-at-office)
* [Экономика офиса, или почему мы все еще ездим на работу](https://rb.ru/story/vr-replaces-offices)
* [Вы работаете не в том месте (если у вас офис открытого типа)](https://habrahabr.ru/post/342344/)
* [В Германию и Голландию сейчас попасть достаточно просто: местные компании нуждаются в кадрах](https://vc.ru/amp/29066)
* [Не путайте разработку ПО и программирование](https://habrahabr.ru/company/alconost/blog/341304/)
* [ВИДЕО: Andrey Listochkin - Качество. Как делать свою работу хорошо.](https://www.youtube.com/watch?v=Mx22NaWmFhk)
* [Пол Грэм. Все статьи на русском. Два года спустя](https://habrahabr.ru/company/philtech/blog/341180/) - очень популярный чувак , пишущий про IT-стартапы
* [Почему вам должно быть скучно на работе](https://habrahabr.ru/post/348614/)

## Рабочий дневник программиста

Полезная привычка - вести дневник работы. Удобно вспомнить чем занимался, отслеживать прогресс, вспоминать проблемы, с которыми сталкивался и т.п.

- [Рабочий дневник программиста](https://habrahabr.ru/post/324320/)
- [Typora](https://typora.io/) - удобный редактор локальных MD- файлов

## GTD

* "Джедайская техника" Максима Дорофеева
    * [Джедайская техника пустого инбокса или как доводить дела до конца](http://sergeysichkar.ru/jedi-indox/)
    * [Принцип экономии мыслетоплива](https://habrahabr.ru/company/oleg-bunin/blog/348714/)
    * [Точки регулировки джедайской техники личной эффективности](http://sergeysichkar.ru/setup-jedi-indox/)
    * [Джедайская техника. MindMap](https://www.mindmeister.com/ru/426089298/_)
* [Пленники самодисциплины. Как мы разучились отдыхать и почему это плохо](https://knife.media/self-discipline/)
* [How to Design a Lifestyle That Eliminates Distraction and Enables Hyper-Focus](https://medium.com/the-mission/how-to-design-a-lifestyle-that-eliminates-distraction-and-enables-hyper-focus-c1f350d5b5ba)

## Книги. Саммари

* [«Программист-прагматик. Путь от подмастерья к мастеру»: коротко о главном (часть первая)](https://habrahabr.ru/company/everydaytools/blog/348116/)
* [«Программист-прагматик. Путь от подмастерья к мастеру»: коротко о главном (часть вторая)](https://habrahabr.ru/company/everydaytools/blog/348932/)
* [Идеальный программист. Часть 1](https://habrahabr.ru/post/317312/)
* [Идеальный программист. Часть 2](https://habrahabr.ru/post/317850/)
* [ Миф о многозадачности](https://habrahabr.ru/post/289494/)

## Обучение

* [План обучения джедаев](http://www.it-agency.ru/academy/jedi-plan/)


### Harvard CS50

* [Перводы видео курса Harvard CS50](https://javarush.ru/quests/QUEST_HARVARD_CS50)
* [Самые сложные и интересные проекты на Scratch](https://itgen.io/samye-slozhnye-i-interesnye-proekty-na-scratch)


