# WorkFlow

[#Home](.././README.md) | [GTD](README.md)

### Atom IDE

Atom - довольно быстрый редактор, но не IDE. Однако, есть попытки двигаться в этом направлении:  
- [atom.io](https://atom.io/)
- [Introducing Atom-IDE](https://blog.atom.io/2017/09/12/announcing-atom-ide.html)
- [Текстовый редактор Atom стал средой разработки](https://tproger.ru/news/atom-ide/)
- [Какими плагинами сделать из Atom IDE для js/php?](https://toster.ru/q/392725)

### Anki

Метод обучения с помощью карточек 
* https://apps.ankiweb.net/
* https://ru.wikipedia.org/wiki/Anki
* [Дерек Сиверс: «Метод интервального повторения — лучший способ изучения языков программирования»](https://habrahabr.ru/post/196448/)
* [Преимущества метода интервального запоминания](https://habrahabr.ru/company/everydaytools/blog/322286/)
* [Помнить все: Evernote и Anki](https://habrahabr.ru/post/240451/)
