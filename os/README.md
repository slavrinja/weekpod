# OS

[#Home](.././README.md)

[TOC]

# Linux

- **16 трюков для консоли Linux**

  - https://proglib.io/p/new-linux-tricks/

  - просто небольшой очереной набор полезных команд

  - мне полезно ``sudo !!`` Запускает предыдущую команду с root правами.

  - а ещё с помощью **file** можно посмотреть кодировку

    - ```bash
      $ file Polish.srt 
      Polish.srt: UTF-8 Unicode text, with CRLF line terminators
      ```

      ​